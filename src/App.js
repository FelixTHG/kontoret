import React, {Fragment} from "react";
import "./index.css";
import "./App.css";
import "./Routes.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Switch } from "react-router-dom/cjs/react-router-dom.min"; //, withRouter
import Showcase from "./comps/Showcase/Showcase";
import SingleShowcase from "./comps/Showcase/SingleShowcase";
import Palette from "./Palette/Palette";


export default function App() {
    //const history = useHistory();

  return (
    <div className="app" style={{backgroundColor: colors.surface}}>
      {/* navbar? */}
      <Router basename="/kontoret"> 
      <main>
        <nav>
          <ul>
            {/* create custom hover function to highlight hovered links */}
            <li style={liStyles}><span className="navButton" style={{backgroundColor: colors.tertiaryBrighter}}><Link style={{color: colors.surface, textDecoration: "none"}} to="/">Showcase</Link></span></li>
            <li style={liStyles}><span className="navButton" style={{backgroundColor: colors.tertiaryBrighter}}><Link style={{color: colors.surface, textDecoration: "none"}} to="/about">About</Link></span></li>
            <li style={liStyles}><span className="navButton" style={{backgroundColor: colors.tertiaryBrighter}}><Link style={{color: colors.surface, textDecoration: "none"}} to="/contact">Contact</Link></span></li>
          </ul>
        </nav>
        <Switch className="shown">
          <Route exact path="/" render={(props)=> <Showcase {...props} colors={colors} />} />
          <Route path="/about" render={(props)=> <About {...props} colors={colors} />} />
          <Route path="/contact"  render={(props)=> <Contact {...props} colors={colors} />} />
          <Route path="/showcase/drums" render={(props)=> <SingleShowcase {...props} colors={colors} />} />
          <Route path="/showcase/instruments" render={(props)=> <SingleShowcase {...props} colors={colors} />} />
          <Route path="/showcase/recording" render={(props)=> <SingleShowcase {...props} colors={colors} />} />
          <Route path="/showcase/placeholder" render={(props)=> <SingleShowcase {...props} colors={colors} />} />
          <Route render={() => <h1>404: page not found</h1>} />
        </Switch>
      </main>
      </Router>
      <footer style={{color: colors.primary}}>Footer</footer>
      <Palette primary={colors.primary} surface={colors.surface} primaryBrighter={colors.primaryBrighter} secondary={colors.secondary} secondaryBrighter={colors.secondaryBrighter} tertiary={colors.tertiary} tertiaryBrighter={colors.tertiaryBrighter} />
    </div>
  );
}

let colors = {
  surface: "#452a00",
  primary: "#57bf3f",
  primaryBrighter: "#62db73",
  secondary: "#c4b400",
  secondaryBrighter: "#c97d24",
  tertiary: "#bd4a24",
  tertiaryBrighter: "#bd4a24",
};

const liStyles = {
  listStyleType: "none",
  display: "inline",
  marginRight: "7px",
}

// About Page
const About = () => (
  <Fragment>
    <h1>About</h1>
    <FakeText />
  </Fragment>
  );
// Contact Page
const Contact = () => (
  <Fragment>
    <h1>Contact</h1>
    <FakeText />
  </Fragment>
  );

const FakeText = () => (
  <p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </p>
  )

//should showcase different elements:
//Mics, Drums, Guitar-gear, Bass-gear, misc?
//producer profiles?



