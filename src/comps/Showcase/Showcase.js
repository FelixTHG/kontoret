import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import "./Showcase.css";
import { showcase } from "./showcases";
//import {useState} from "react";

const Showcase = (props) => {
  const history = useHistory();

  return (
    <div>
      <h1 style={{color: props.colors.primary}}>Our Gear, Your enjoyment!</h1>
      <h2 style={{color: props.colors.primary}}>Take a look:</h2>
      <div className="frontPageShowcaseContainer" style={{backgroundColor: props.colors.secondary}}>
        {showcase.map((showcase) =>(
          <div key={showcase.id} className="frontPageShowcase" onClick={() => { history.push(`/showcase/${showcase.title}`)}}>
            <div className="imgContainer" style={{}}><span className="imgText" style={{color: props.colors.primary}}>{showcase.title}</span><img src={showcase.image} alt="Drum with logo"></img></div>
          </div>
        ))}
        {/* 
        create components for each showcase, and give them a route each
        summarize the above components in this container
        */}
      </div>
    </div>
  )
}

export default Showcase