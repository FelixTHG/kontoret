//import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import "./Showcase.css";
import { SingleShowcases} from "./showcases";
//import {useState} from "react";

const SingleShowcase = (props) => {
  //const history = useHistory();
  //make singleshowcase 
  //need to figure out how to deal with standing images
  return (
    <div>
      <h1 style={{color: props.colors.primary}}>SingleShowcasePage</h1>
      <h2 style={{color: props.colors.primary}}>Take a look:</h2>
      <div className="singleShowcaseContainer" style={{backgroundColor: props.colors.tertiary}}>
        {SingleShowcases.filter((showcase) => showcase.category === props.location.pathname.split("/")[2]).map((showcase) =>(
          <div key={showcase.id} className="singleShowcase">
            <div className="singleShowcaseImgContainer" style={{}}><img src={showcase.image} alt="Drum with logo (make dynamic)"></img></div>
            <p className="desc" style={{color: props.colors.primary}}>{showcase.description}</p>
          </div>
        ))}
      </div>
    </div>
  )
}

export default SingleShowcase