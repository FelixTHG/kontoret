import "./Palette.css";
import PropTypes from "prop-types";

const Palette = ({surface, primary, primaryBrighter, secondary, secondaryBrighter, tertiary, tertiaryBrighter}) => {
  return (
   <div className="paletteContainer">
     <div className="square primary" style={{backgroundColor: primary}}></div>
     <div className="square surface" style={{backgroundColor: surface}}></div>
     <div className="square primaryBrighter" style={{backgroundColor: primaryBrighter}}></div>
     <div className="square secondary" style={{backgroundColor: secondary}}></div>
     <div className="square secondaryBrighter" style={{backgroundColor: secondaryBrighter}}></div>
     <div className="square tertiary" style={{backgroundColor: tertiary}}></div>
     <div className="square tertiaryBrighter" style={{backgroundColor: tertiaryBrighter}}></div>
   </div>
  )
}

Palette.defaultProps = {
  surface: "#121722",
  primary: "#52b262",
  primaryBrighter: "#62db73",
  secondary: "#b25262",
  secondaryBrighter: "#db6273",
  tertiary: "#6252b2",
  tertiaryBrighter: "#7362db",
};

Palette.propTypes = {
  surface: PropTypes.string,
  primary: PropTypes.string,
  primaryBrighter: PropTypes.string,
  secondary: PropTypes.string,
  secondaryBrighter: PropTypes.string,
  tertiary: PropTypes.string,
  tertiaryBrighter: PropTypes.string,
}



export default Palette